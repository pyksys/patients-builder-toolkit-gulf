type TProfile = {
  name: string;
  age: number;
  gender: string;
  photo?: {
    small: unknown;
    big: unknown;
  };
};

export type TAppropriate = {
  type: string;
  count: number;
  unit: string;
  upTo: boolean;
  info: {
    default: {
      title: string;
      description: string;
    };
    important: {
      title: string;
      description: string;
    };
  };
  graph?: unknown;
};

type TType = {
  typeId: number | string;
  title: string;
  about: Array<string>;
  titleModal?: string;
  bodyModal?: string;
  appropriate: Readonly<TAppropriate[]>;
  enrolled?: string;
};

type TPatient = {
  profile?: Readonly<TProfile>;
  diagnosis: Array<string>;
  proposal: Array<string>;
  medical: Array<{ prop: string; value: string }>;
};

export type PatientData = Readonly<{
  id: string;
  title: string;
  type: Readonly<TType>;
  patient: Readonly<TPatient>;
}>;

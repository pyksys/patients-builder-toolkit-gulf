# emergin-markets

Application diagrams:
https://app.diagrams.net/#G1rSVoOJHCErdrZ9HcnHePyIogI2DRdv_f

## Building
To build the iOS project make a push to 'build/ios' branch.

[AppCenter page](https://appcenter.ms/orgs/Bully/apps/Asthma-EM)

[Install link](https://install.appcenter.ms/orgs/Bully/apps/Asthma-EM/distribution_groups/devs)

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

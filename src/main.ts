import Vue from 'vue';
import App from './App.vue';
import './registerServiceWorker';
import router from './router';
import AppButton from '@/components/app/AppButton.vue';

Vue.config.productionTip = false;

Vue.component('app-button', AppButton);

new Vue({
  router,
  render: (h) => h(App),
}).$mount('#app');
import { Material, Color } from 'three';

/**
 * @author mrdoob / http://mrdoob.com/
 * @author alteredq / http://alteredqualia.com/
 *
 * parameters = {
 *  color: <hex>,
 *  opacity: <float>,
 *  map: new THREE.Texture( <Image> ),
 *
 *  size: <float>,
 *  sizeAttenuation: <bool>
 *
 *  morphTargets: <bool>
 * }
 */

function PointsMaterial(parameters) {
  Material.call(this);

  this.type = 'PointsMaterial';

  this.color = new Color(0xffffff);

  this.map = null;

  this.size = 1;
  this.sizeAttenuation = true;

  this.morphTargets = false;
  this.skinning = false;

  this.lights = false;

  this.setValues(parameters);
}

PointsMaterial.prototype = Object.create(Material.prototype);
PointsMaterial.prototype.constructor = PointsMaterial;

PointsMaterial.prototype.isPointsMaterial = true;

PointsMaterial.prototype.copy = function (source) {
  Material.prototype.copy.call(this, source);

  this.color.copy(source.color);

  this.map = source.map;

  this.size = source.size;
  this.sizeAttenuation = source.sizeAttenuation;

  this.morphTargets = source.morphTargets;
  this.skinning = source.skinning;

  return this;
};

export { PointsMaterial };

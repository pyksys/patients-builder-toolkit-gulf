#include <packing>

uniform vec3 diffuse;
uniform float opacity;
varying float vSize;
uniform sampler2D tColor;
uniform sampler2D tDepth;
uniform vec2 resolution;
varying float phase;
varying vec3 random;
varying float need_hide;

uniform float cameraNear;
uniform float cameraFar;
uniform float zthreshold;
uniform float coef;
uniform float ocs_disable;

uniform float huee;

const vec4  kRGBToYPrime = vec4 (0.299, 0.587, 0.114, 0.0);
const vec4  kRGBToI     = vec4 (0.596, -0.275, -0.321, 0.0);
const vec4  kRGBToQ     = vec4 (0.212, -0.523, 0.311, 0.0);

const vec4  kYIQToR   = vec4 (1.0, 0.956, 0.621, 0.0);
const vec4  kYIQToG   = vec4 (1.0, -0.272, -0.647, 0.0);
const vec4  kYIQToB   = vec4 (1.0, -1.107, 1.704, 0.0);

vec4 hue_shift(vec4 color, float hue_ofst)
{
    float   YPrime  = dot (color, kRGBToYPrime);
    float   I      = dot (color, kRGBToI);
    float   Q      = dot (color, kRGBToQ);

    // Calculate the chroma
    float   chroma  = sqrt (I * I + Q * Q);

    // hue angel
    float   hue     = atan (Q, I);

    // Convert desired hue back to YIQ
    Q = chroma * sin (hue + hue_ofst);
    I = chroma * cos (hue + hue_ofst);

    // Convert back to RGB
    vec4    yIQ   = vec4 (YPrime, I, Q, 0.0);
    color.r = dot (yIQ, kYIQToR);
    color.g = dot (yIQ, kYIQToG);
    color.b = dot (yIQ, kYIQToB);
	return color;
}

#ifdef ANIMATED
uniform float multiplier;
#endif

float toLinear(float depth, float near, float far)
{
	float z = depth * 2.0 - 1.0;
	float linear = (2.0 * far * near) / (far + near - z*(far-near));
	return linear;
}

#include <common>
#include <color_pars_fragment>
#include <map_particle_pars_fragment>
#include <fog_pars_fragment>
#include <logdepthbuf_pars_fragment>
#include <clipping_planes_pars_fragment>

void main() {

	#include <clipping_planes_fragment>

	vec3 outgoingLight = vec3( 0.0 );
#ifdef ANIMATED
	// `phase` is used to scale up particle size.
	// The more scale the less opacity should be.
    // float factor = (phase + 1.0) * phase + 0.3;
    float factor = phase*phase + 0.3; // phase + 0.3;
	float op = clamp((1.0 - factor + 0.1) * opacity * coef, 0.0, 1.0);
	vec4 diffuseColor = vec4( diffuse, op);
#else
	vec4 diffuseColor = vec4( diffuse, opacity );
#endif

	#include <logdepthbuf_fragment>
	// #include <map_particle_fragment>
	vec2 _uv = gl_FragCoord.xy/resolution;
    vec4 raw_color = texture2D( tColor, _uv );
    float raw_depth = texture2D( tDepth, _uv ).x;
    float depth = toLinear( raw_depth, cameraNear, cameraFar );
    float fragDepth = toLinear( gl_FragCoord.z, cameraNear, cameraFar );

#ifdef ANIMATED
	float offset = 0.5 * step(0.5, phase);
	float width = 0.5;
	vec2 uv = ( uvTransform * vec3( gl_PointCoord.x, 1.0 - gl_PointCoord.y, 1 ) ).xy;
	uv.x *= 0.5;
	uv.x += offset;

	vec4 mapTexel = texture2D( map, uv );
	diffuseColor *= mapTexelToLinear( mapTexel );

	float f = fract(sin(phase)*1000.0);
	float z = depth/(cameraFar - cameraNear);
	#ifdef EXTERNAL
		diffuseColor.a *= 1.0 - step(0.0, 1.0 - z);
	#else
		diffuseColor.a *= step(0.0, 1.0 - z);
	#endif
	#ifndef IGNORECOLORMAP
		diffuseColor.rgb = raw_color.rgb;
		diffuseColor.rgb *= 7.25;
	#endif
	diffuseColor.a *= smoothstep(f-0.1, f+0.1, multiplier);
	
#else
	vec2 uv = ( uvTransform * vec3( gl_PointCoord.x, 1.0 - gl_PointCoord.y, 1 ) ).xy;

	float halo_point_value = 0.5;
	float halo_point_filter = step(halo_point_value - 0.1, need_hide) * step(need_hide, halo_point_value + 0.1);
	float ocs_point_value = 0.3;
	float ocs_point_filter = step(ocs_point_value - 0.1, need_hide) * step(need_hide, ocs_point_value + 0.1);

	float sizer = clamp((vSize - 15.) / 10.0, 0.0, 1.0);
	sizer = mix(1.0, sizer, ocs_point_filter); // sizer only affects ocs points

	float dist = length(uv - vec2(0.5));

	float dot_rad = 0.07 * sizer;

	float vc = 1.0 - smoothstep(dot_rad - fwidth(dist)*sizer, dot_rad + fwidth(dist)*sizer, dist);
	float vs = 1.0 - smoothstep(0.0, 0.5, dist)  ;

	float v1 = max(vc * 0.45, vs * 0.05) ; // halo poitn - HACK alphas is busted because of multible drawes
	float v2 = vc * 0.45; // simple point - same HACK here

	// Choose which type of point to show
    diffuseColor.a = mix(v2, v1, halo_point_filter);

	// Show additional dots when OCS is YES.
    //diffuseColor.a *= 1.0 - step(0.1, need_hide) * step(need_hide, 0.35) * ocs_disable;

	vec3 dc = diffuseColor.rgb;
	dc = mix(dc, clamp(dc + (1.0-dc)*0.3, 0.0 , 1.0), diffuseColor.a * 2.0 * halo_point_filter);
	diffuseColor.rgb = dc;


	float fst = random.x;
	fst *= (1.0 - smoothstep(1.7, 2.5, huee));
	diffuseColor = hue_shift(diffuseColor, huee + fst);
#endif

	#include <color_fragment>
	#include <alphatest_fragment>

	outgoingLight = diffuseColor.rgb;

#ifndef ANIMATED
	diffuseColor.a *= step(0.2, need_hide); // show only points marked with 0.3, 0.5, and 1.0 vertex color.
#endif

	gl_FragColor = vec4( outgoingLight, diffuseColor.a );

	#include <premultiplied_alpha_fragment>
	#include <tonemapping_fragment>
	#include <encodings_fragment>
	#include <fog_fragment>
}

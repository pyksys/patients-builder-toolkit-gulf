uniform float size;
uniform float scale;
uniform sampler2D tDepth;
attribute float hide;
// attribute float hide_indices;
varying float need_hide;
varying float vSize;
uniform vec2 resolution;

attribute vec4 color;
attribute vec3 randomy;
varying vec3 random;
uniform float huee;

#ifdef ANIMATED
uniform float time;
uniform float freq;
uniform float amplitude;
attribute float P1; //Phase
varying float phase; //Phase
#endif

#include <common>
#include <color_pars_vertex>
#include <fog_pars_vertex>
#include <morphtarget_pars_vertex>
#include <skinning_pars_vertex>
#include <logdepthbuf_pars_vertex>
#include <clipping_planes_pars_vertex>

void main() {

#include <color_vertex>
#include <begin_vertex>
#include <skinbase_vertex>
#include <morphtarget_vertex>
#include <skinning_vertex>

    // transformed += normal;
#ifdef ANIMATED
    float p = P1 * 15.0;
    float _x = amplitude * cos(freq*time + p);
    float _y = amplitude * cos(freq*time + p+p);
    float _z = amplitude * cos(freq*time + p);
    transformed += vec3(_x, _y, _z);
#endif

#include <project_vertex>
    //float oo = step(0.4, color.x) * step(color.x, 0.6);
    gl_PointSize = size / 600.0 * resolution.y;
#ifdef ANIMATED
    gl_PointSize *= 2.0 * (P1 + 1.0) * P1 + 0.3;
#else
    gl_PointSize *= 3.0;
#endif

#ifdef USE_SIZEATTENUATION
    bool isPerspective = ( projectionMatrix[ 2 ][ 3 ] == - 1.0 );
    if ( isPerspective ) gl_PointSize *= ( scale / - mvPosition.z );
#endif
#ifdef USE_SKINNING
    //gl_PointSize = 0.1;
#endif
#include <logdepthbuf_vertex>
#include <clipping_planes_vertex>
#include <worldpos_vertex>
#include <fog_vertex>
#ifdef ANIMATED
    phase = P1;
#endif
	random = randomy;
	vec4 wp = modelMatrix * vec4( transformed, 1.0 );

    // Variate randomyx in world X direction
	random.x = fract(random.x + huee/2.5);
	random.x = ((random.x - 0.5) * 10.0 + 0.5);
	float rgrad = mix(random.x, (0.0 + (wp.x ) ) * 0.15 + 0.5, 0.8);
	rgrad = rgrad * (sin( mix(0.0, 3.14, huee / 2.5) ) );
	random.x = clamp(rgrad, 0.0, 1.0);
    // highp int index = int(hide_indices);
    need_hide = color.x;
    vSize = size;
    // need_hide = hide;
}

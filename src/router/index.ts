import Vue from 'vue';
import VueRouter, { RouteConfig } from 'vue-router';
import Home from '../views/Home.vue';

Vue.use(VueRouter);

function onHashChange() {
  console.log(document.location.hash);
}

const routes: Array<RouteConfig> = [
  {
    path: '/',
    name: 'Home',
    component: Home,
    meta: {
      report: "Home"
    }
  },
  {
    path: '/build',
    name: 'Build',
    // redirect: '/',
    component: Home
  },
  {
    path: '/patient/:typeId',
    name: 'Patient',
    component: () => import('@/views/Patient.vue')
  },
  {
    path: '/patient',
    redirect: '/'
  },
  {
    path: '/prescribing',
    name: 'PrescribingInformation',
    component: () => import('@/views/PrescribingInformation.vue'),
    meta: {
      report: "Prescribing Information"
    }
  },
  {
    path: '/types',
    name: 'Type2Asthma',
    component: () => import('@/views/Type2Asthma.vue'),
    meta: {
      report: "Type 2 Asthma"
    }
  },
  {
    path: '/summary',
    name: 'Summary',
    component: () => import('@/views/Summary.vue'),
    meta: {
      report: "Summary"
    }
  },
  {
    path: '/safety',
    name: 'SafetyData',
    component: () => import('@/views/SafetyData.vue'),
    meta: {
      report: "Safety Data"
    }
  },
  {
    path: '/dosing',
    name: 'Dosing',
    component: () => import('@/views/Dosing.vue'),
    meta: {
      report: "Dosing"
    }
  },
  {
    path: '/design',
    name: 'StudyDesign',
    component: () => import('@/views/StudyDesign.vue'),
    meta: {
      report: "Study Design"
    }
  },
  {
    path: '/references',
    name: 'References',
    component: () => import('@/views/References.vue'),
    meta: {
      report: "References"
    }
  },
  {
    path: '/builder',
    name: 'Builder',
    component: () => import('@/views/Builder.vue'),
  },
  {
    path: '*',
    redirect: '/',
  },
];

const router = new VueRouter({
  routes,
});

router.afterEach((to, from) => {
  if (JSON.stringify(to.meta) !== "{}") {
    try {
      window.parent.onEnterPage(to.meta.report);
    } catch (error) {
      console.log("Reporting: " + to.meta.report)
    }
  }
});

export default router;
